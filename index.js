const Redis = require('ioredis');

class LokiRedisAdapter {
  constructor(redisurl) {
    if (typeof redisurl === 'string') this.redis = new Redis(redisurl);
    else this.redis = new Redis();
  }

  loadDatabase(dbname, cb) {
    this.redis.get(dbname).then((data) => cb(data));
  }

  saveDatabase(dbname, dbstring, cb) {
    this.redis.set(dbname, dbstring).then(() => cb());
  }
}

module.exports = LokiRedisAdapter;
